# Proto_board
Proto_board is a dual-purpose prototyping shield. 
Not only does it have a large 0.1" grid prototyping area but it also extends the USB pins to USB port.


<img src="pic_main.png" width="500">
<img src="show_off.png" width="500">
<img src="expention_board_top.png" width="500"> 
<img src="expention_board_bottom.png" width="500"> 